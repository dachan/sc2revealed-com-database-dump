This is a data dump from [sc2revealed.com](https://sc2revealed.com/) as of June 10, 2024.

### True IDs (true_ids)

Identified players with country, race , social media links and misc ladder data

### Battlenet Profiles (battlenet_profiles)

Mapping of Battle.net URIs to high-level players

### Aliases (aliases)

Mapping of Character IDs to True IDs

### Misc ladder data

Ladder data from the past 3-4 (?) years
